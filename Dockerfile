FROM node:8.14.0-jessie

WORKDIR /usr/src/hello-world-docker

COPY . .

RUN npm install

EXPOSE 8080/tcp

CMD [ "node", "index.js" ]
